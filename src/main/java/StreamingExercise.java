import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

public class StreamingExercise {

    /**
     * Working with streamingAPI
     */
    public static void demonstrateStreaming() {

        //Filters the stringList in order to remove the empty strings

        List<String> stringList = Arrays.asList("abs", "", "dknv", "", "jbdv");
        List<String> filtered = stringList.stream().filter(string -> !string.isEmpty()).collect(Collectors.toList());

        //Counts the empty strings in the stringList

        int count = (int) stringList.stream().filter(string -> string.isEmpty()).count();

        //Creates a sorted list of 1000 random integers

        Random random = new Random();
        List<Integer> randomNums = random.ints().limit(1000).sorted().boxed().collect(Collectors.toList());

       //Calculates the max, min, average and sum of all the integers in the list

        int max = randomNums.stream().mapToInt(i -> i).max().getAsInt();
        System.out.println("The maximum number is " + max);

        int min = randomNums.stream().mapToInt(i -> i).min().getAsInt();
        System.out.println("The minimum number is " + min);

        double avg = randomNums.stream().mapToInt(i -> i).average().getAsDouble();
        System.out.println("The average number is " + avg);

        int sum = randomNums.stream().mapToInt(ft -> ft.intValue()).sum();
        System.out.println("The sum of all the numbers is " + sum);

       //Creates a list of the odd numbers and a list of the even ones.

        List<Integer> oddList = randomNums.stream().filter(i -> i % 2 != 0).collect(Collectors.toList());

        List<Integer> evenList = randomNums.stream().filter(i -> i % 2 == 0).collect(Collectors.toList());
    }
}
